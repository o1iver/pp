# -*- coding: utf-8 -*-
"""
    pp
    ~~

    :copyright: (c) 2012 by Oliver Stollmann
    :license: BSD3
"""

import os
import sys
import time
import argparse

# ------------------------------------------------------------------------------
# | Module management

class Module(object):
    """
    Represents a python module (i.e. a python source code file).

    :param name: Module name (e.g. mymodule.util)
    :param base_dir: Custom base directory.
    :param content: Content of the module file. Default: None.
    :param auto_read: Whether or not to automatically fill the module contents
                      by reading the source file. Default: True. Note that
                      defining custom content will override the auto_read
                      property on initialization.

    """

    @staticmethod
    def from_file(base_dir,path):
        """
        Create a Module instance from a given file.
        :param base_dir: Path to project directory.
        :param path: Path from project directory to module file.

        """
        full_path = os.path.join(base_dir,path)

        if path.startswith('/'):
            p_err('Path for Module.from_file must be relative to base_dir.')
            wait_exit(1)

        if not path.endswith('.py'):
            p_dbg('Path for Module.from_file is not a *.py file.')
            return

        # Get module name from path
        parts = path.split('/')
        parent_modules = parts[:-1]     # Get parents
        actual_module  = parts[-1][:-3] # Get actual and remove .py
        module_name = '.'.join(parent_modules + [actual_module])

        p_inf("File `%s' might define module `%s'." % (path,module_name))

        module = Module(name=module_name,base_dir=base_dir,auto_read=True)
        return module


    def __init__(self,name,base_dir=None,content=None,auto_read=None):
        # Module name (e.g. twisted.util)
        self.name = name
        # Custom base directory
        self.base_dir = base_dir or ''

        auto_read = auto_read if auto_read is not None else True

        # Make sure auto_read is disabled if custom contents have been given.
        if content is not None and auto_read:
            p_wrn("Auto read will not occur because custom content has been " +
                  "defined.")
            auto_read = False

        # module file content
        self.content = content if not auto_read else self.read()

    def read(self):
        """
        Return the content of this module's source file.

        :returns: Contents of file at self.path.
        :rtype: str
        """

        p_dbg("Reading contents of module `%s' (%s)." % (self.name,self.path))
        with open(self.path,'r') as f:
            content = f.read()
        return content

    @property
    def path(self):
        """
        Path to the source code file of this module.

        >>> m = Module("this.is.a.module",auto_read=False)
        >>> m.path
        'this/is/a/module.py'

        >>> m = Module("a",auto_read=False)
        >>> m.path
        'a.py'

        >>> m = Module("a",base_dir='/home/dev/',auto_read=False)
        >>> m.path
        '/home/dev/a.py'
        """
        name_parts = self.name.split('.')
        return os.path.join(self.base_dir,*name_parts) + ".py"

    @property
    def exists():
        """
        Checks whether the file returned by self.path() exists.

        :returns: Whether the module source file exists.
        :rtype: bool
        """
        return os.path.isfile(self.path())
    
    def make_package(self):
        """
        Transform a python module into a python package. The most common case
        for this is likely to be when a module must be submoduled, meaning
        that even though module `a' exists we want to now define a module `a.b'
        and for that to be possible `a' must be transformed into a package that
        can then contain submodules such as `a.b'.

        Note: that what is currentely accessable using `a.<something>' will
        still be accessable as the source code from the module will be available
        in the initalization file after this transformation.
        """
        pass

class Package(object):
    """
    Represents a python package (i.e. a folder containing a package
    initialization file and possibly a number of sub-modules).

    A package can contain either modules or packages (these are the package's
    children).

    :param name: Package name.
    :type  name: str

    :param base_dir: Optional custom base directory.
    :type  base_dir: str

    :param init_content: Optional content of initialization file.
    :type  init_content: str

    :param auto_read: Optional specification of whether or not the contents of
                      the packages initialization (i.e. __init__.py) file should
                      automatically be read. Default: True.
                      Note: This value is ignored if init_content if not None
                            (i.e. is custom content is passed, the file will not
                            be automatically read).
    :type  auto_read: bool

    :param children: Package children (can be modules or packages).
    :type  children: list


    """
    @staticmethod
    def from_directory(base_dir,path,recursive_find_children=True):
        directory_path = os.path.join(base_dir,path)
        init_file_path = os.path.join(base_dir,path,"__init__.py")

        # Check if directory contains initialization file.
        if not os.path.isfile(init_file_path):
            p_dbg("Directory `%s' cannot be a package as it does not contain " \
                  "an initialization (__init__.py) file." % directory_path)
            return

        # Get module name from path
        parts = path.split('/')
        parent_packages = parts[:-1] # Get parent packages
        actual_packages  = parts[-1] # Get current package
        package_name = '.'.join(parent_packages + [actual_packages])

        p_inf("Directory `%s' defines package `%s'." % (path,package_name))

        package = Package(name=package_name
                         ,base_dir=base_dir
                         ,init_content=None
                         ,auto_read=True
                         ,children=None
                         ,recursive_find_children=recursive_find_children)

        return package



    def find_children(self):
        modules  = []
        packages = []

        path = self.path
        for item in os.listdir(path):
            if item == "__init__.py":
                continue
            item_path = os.path.join(path,item)
            if   os.path.isdir(item_path):
                # Possible package directory
                p_dbg("Currently looking at directory `%s'." % item_path)
                package = Package.from_directory(path,item)
                if package:
                    packages.append(package)
                    p_inf("Found package `%s' at `%s'." % (package.name
                                                          ,package.path))
                else:
                    p_inf("No package in `%s'." % item_path)

            elif os.path.isfile(item_path):
                # Possible module file
                p_dbg("Currently looking at file `%s'." % item_path)
                module = Module.from_file(path,item)
                if module:
                    modules.append(module)
                    p_inf("Found module `%s' at `%s'." % (module.name,module.path))
                else:
                    p_inf("No module defined in `%s'." % item_path)

        p_inf("Found %u child module(s)."  % len(modules))
        self.children.extend(modules)

        p_inf("Found %u child package(s)." % len(packages))
        self.children.extend(packages)


    def __init__(self,name,base_dir=None,init_content=None,auto_read=None
                ,children=None,recursive_find_children=None):
        # Package name (e.g. twisted.util)
        self.name = name
        # Custom base directory
        self.base_dir = base_dir or ''

        if not children and recursive_find_children:
            self.children = []
            self.find_children()
        else:
            self.children = children

        auto_read = auto_read if auto_read is not None else True

        # Make sure auto_read is disabled if custom contents have been given.
        if init_content is not None and auto_read:
            p_wrn("Auto read will not occur because custom content has been " +
                  "defined.")
            auto_read = False

        # module file content
        self.init_content = init_content if not auto_read else self.read_init()

    def read_init(self):
        """
        Return the content of this package's initialization file.

        :returns: Contents of file at self.init_path.
        :rtype: str
        """

        p_dbg("Reading contents of package `%s' initialization file (%s)." % (
                                                    self.name,self.init_path))
        with open(self.init_path,'r') as f:
            content = f.read()
        return content

    @property
    def path(self):
        """
        Path to the directory containing this package.

        >>> p = Package("this.is.a.package",auto_read=False)
        >>> p.path
        'this/is/a/package/'

        >>> p = Package("a",auto_read=False)
        >>> p.path
        'a/'

        >>> p = Package("a","/home/ost/",auto_read=False)
        >>> p.path
        '/home/ost/a/'
        """

        name_parts = self.name.split('.')
        name_parts.append('') # append '' to list to get trailing slash
        return os.path.join(self.base_dir,*name_parts)

    @property
    def init_path(self):
        """
        Path to a package's initialization file.

        >>> p = Package("this.is.a.package",auto_read=False)
        >>> p.init_path
        'this/is/a/package/__init__.py'

        >>> p = Package("a","/root/dev/",auto_read=False)
        >>> p.init_path
        '/root/dev/a/__init__.py'
        """
        name_parts = self.name.split('.')
        name_parts.append("__init__.py")
        return os.path.join(self.base_dir,*name_parts)
    
    @property
    def exists():
        """
        Checks whether the file returned by self.path() exists.

        :returns: Whether the module source file exists.
        :rtype: bool
        """
        return os.path.isdir(self.path) and os.path.isfile(self.init_path)


class Project(object):
    @staticmethod
    def from_directory(path):
        """
        Creates a project using the first package it finds in the given directory.
        """
        if not os.path.isdir(path):
            p_err("Given path `%s' is not a valid directory." % path)
            wait_exit(1)

        package = None
        # Loop through items looking for package
        for item in os.listdir(path):
            item_path = os.path.join(path,item)
            if os.path.isdir(item_path):
                package = Package.from_directory(path,item)
                if package:
                    break
        if not package:
            p_err("Unable to find python package in given directory.")
            wait_exit(1)

        return Project(package.name,package)

    def __init__(self,name,package=None):
        self.name = name
        self.package = package

    def show_hierarchy(self):
        """
        Returns a pretty-printed string of the package/module hierarchy of this
        project.
        :returns: pretty-printed string
        :rtype: str
        """
        str_ = self.package.name + '\n'
        prefix = "|-- "
        prefix_len = len(prefix)
        def do(children,depth=1):
            if   depth == 0: p = ''
            elif depth == 1: p = prefix
            else           : p = ' '*prefix_len*(depth-1) + prefix
            str_ = ''
            for child in children:
                str_ += p + child.name + '\n'
                if type(child) == Package and child.children:
                    str_ += do(child.children,depth=depth+1)
            return str_

        str_ += do(self.package.children,depth=1)
        return str_

# ------------------------------------------------------------------------------
# | Utilities 

P_DEBUG = False
P_COLORS = False
native = "\033[0;0m"
green  = "\033[01;32m"
red    = "\033[01;31m"
yellow = "\033[01;33m"

def p_err(str_,newline=True):
    if P_COLORS:
        str_ = red + str_ + native
    str_ = "[E] " + str_
    return p(str_,sys.stderr,newline)

def p_wrn(str_,newline=True):
    if P_COLORS:
        str_ = red + str_ + native
    str_ = "[W] " + str_
    return p(str_,sys.stderr,newline)

def p_inf(str_,newline=True):
    if P_COLORS:
        str_ = green + str_ + native
    str_ = "[I] " + str_
    return p(str_,sys.stdout,newline)

def p_dbg(str_,newline=True):
    str_ = "[D] " + str_
    if P_DEBUG:
        return p(str_,sys.stdout,newline)

def p(str_,fh,newline=True):
    # Append a newline if it is missing
    if newline:
        str_ = str_ if str_[-1] == '\n' else str_ + '\n'
    sys.stderr.write(str_)

def wait_exit(code,wait_time=3):
    """
    Wait for a certain amount of time and then exists with the given status
    code.

    :param code: exit code
    :param wait_time: time to wait before exit in seconds (default: 3)
    """
    p_inf("Exiting...")
    time.sleep(3)
    sys.exit(code)





# ------------------------------------------------------------------------------
# | Main
def main():
    # Run tests before anything else
    import doctest
    doctest.testmod()

    # Root parser
    parser = argparse.ArgumentParser(description="pp")
    parser.add_argument("--debug","-d",action="store_true")

    # Add parsers for sub-commands. Name of the subcommand will be stored in
    # `command'.
    subparsers = parser.add_subparsers(
                         help="Possible sub-commands"
                        ,dest="command")

    # Subparser: `new-module' 
    p_new_module = subparsers.add_parser(
                         "new-module"
                        ,help="create a new module")
    p_new_module.add_argument(
                         "name"
                        ,metavar="<name>")
    p_new_module.add_argument(
                         "--as-directory"
                        ,help="create a <module>/__init__.py style module instead of a simple <module>.py file"
                        ,action="store_true")

    p_new_module.add_argument(
                         "--custom-header-file"
                        ,metavar="<path>"
                        ,help="path to custom header file (jinja2 template)"
                        ,type=str)

    # Subparser: `add-author'
    p_add_author = subparsers.add_parser(
                         "add-author"
                        ,help="add an author to the project")
    p_add_author.add_argument(
                         "name"
                        ,metavar="<name>")
    print(parser.parse_args())

if __name__ == "__main__":
    main()

